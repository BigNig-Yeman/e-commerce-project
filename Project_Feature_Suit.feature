Feature: Creating an account
    In order to create an account
    As a customer or a content creator
    I need to be able to register my information

  Scenario:
    Given I have entered a username that hasn't been used
    And I entered a name and full name
    And I entered a valid password
    And the re-entered password matches the original password
    And I checked the customer or creator radio button
    When I press the sign in button 
    Then I should receive a "Account Created" message
    And my account should be added in the database

  Scenario:
    Given I have entered a username that hasn't been used
    And I entered a name and full name
    And I entered a unvalid password
    And the re-entered password matches the original password
    And I checked the customer or creator radio button
    When I press the sign in button 
    Then I should receive a "Password Incorrect" message

  Scenario:
    Given I have entered a username that hasn't been used
    And I entered a name and full name
    And I entered a valid password
    And the re-entered password does not matches the original password
    And I checked the customer or creator radio button
    When I press the sign in button 
    Then I should receive a "Password Incorrect" message

  Scenario:
    Given I have entered a username that hasn't been used
    And I entered a name and full name
    And I entered a valid password
    And the re-entered password matches the original password
    And I didn't checked the customer or creator radio button
    When I press the sign in button 
    Then I should receive a "Please Choose type of Account" message

  Scenario:
    Given I have entered a username that has been used
    When I press the sign in button 
    Then I should receive a "Username Unavailable" message


Feature: Logging Into Account
    In order to log into an Account
    As a customer or a content creator
    I need to be able to input the correct password for a valide username 
    
  Scenario:
    Given I have entered a correct username 
    And I entered a correct password that belongs to that username
    When I press the sign in button
    Then I should be able to continue on to the next page as loged in

  Scenario:
    Given I have entered a correct username
    And I entered an incorrect password 
    When I press the sign in button 
    Then I Should recieve a "Incorrect Username or Password" message

  Scenario:
    Given I have entered an incorrect username 
    And I have entered a correct password
    When I press the sign in button 
    Then I should recieve a "Incorrect Username or Password"

  Scenario:
    Given I have not enterd a Username
    And I have not entered a Password
    When I press the sign in button 
    Then I should recieve a "Enter username and Password" message


Feature: Setting a profile picture
    In order to set a profile picture
    As a customer or a content creator
    I need to able to select an image from a file chosen by user

  Scenario:
    Given I have an image to set as profile picture
    When I press the add picture button
    Then I should be able to see my files to search for an image




Feature: Search for specific content creator
    In order to search for a specific content creator
    When I press the search bar
    And type for a specific content creator 


Feature: Setting a user name

Feature: Subscribing to content creator

Feature: Unsubscribing to content creator

Feature: Commenting on Content Creator page

Feature: Rating a content creator

Feature: Record Card Info

Feature: Enable or disable automatic subscription

Feature: Perform a Monthly or Yearly payment

Feature: Modify Content Creator's own page
  In order to change the look of my page
  As a content creator
  I should change the appropriate settings in the setting page

  Scenario:
    Given I am on the content creator setting page
    And I am the owner content creator
    When I click the "Backround" setting
    And I select a color available
    And I press on the confirm button
    Then the background color of my page should change

Feature: Add content to own page
  In order to add content on my page
  As a content creator
  I should insert an appropriate file

  Scenario:
    Given I am on the content creator page
    And I am the owner content creator
    When I click on the + button
    And I choose an appropriate file
    And I click on the confirm button
    Then my page should have the new content added to it
    And all my subscribers should be able to view it

Feature: Put content description on your own page
  In order to describe the content that is hidden
  As a content creator
  I should enter the information through my page

  Scenario:
    Given I am on the content creator page
    And I am the owner content creator
    When I click the "More" button of a content on my page
    And I select "Add Description"
    And I enter information in the text box 
    And I press save
    Then the text should appear under the content for everyone

Feature: Add free preview of your page
  In order to add a content as a free preview
  As a content creator
  I need to select and confirm which content will become available

  Scenario:
    Given I am on the content creator page
    And I am the owner content creator
    When I click the "More" button of a content on my page
    And I select "Turn into preview"
    And I press "Yes" on the pop-up page 
    Then the content should be available to everyone, even if they aren't subcribed
 Scenario:
    Given I am on the content creator page
    When I click the "More" button of a content on my page
    And I select "Turn into preview"
    And I press "No" on the pop-up page 
    Then the content shouldn't become free

Feature: Delete a free preview of your page
  In order to add a content as a free preview
  As a content creator
  I need to select an available content and confirm that I want it removed
  
  Scenario:
    Given I am on the content creator page
    And I am the owner content creator
    And I select a content that was added as a preview
    When I click the "More" button of a content on my page
    And I select "Remove from preview"
    And I press "Yes" on the pop-up page 
    Then the content shouldn't be available anymore
  Scenario:
    Given I am on the content creator page
    And I select a content that was added as a preview
    When I click the "More" button of a content on my page
    And I select "Remove from preview"
    And I press "No" on the pop-up page 
    Then the content should stay available for everyone

Feature: Receive request from subscribers
  In order to receive request from a subscribers
  As a content creator 
  I need to access my information page

  Scenario:
    Given I am on the content creator information page
    And I am the owner content creator
    When I click the message tab
    Then a message box with messages from customers should appear

Feature: View the amount of subscribers 
  In order to view the amount of subscribers I have
  As a content creator or client
  I need to access my information page

  Scenario:
    Given I am on the content creator information page
    When I scroll through the list of information
    Then I should see a row called "Subscriber count" and the number of subscribers they have have

Feature: Set and change subscription price for months that are 2 months away
  In order to change the price of my subscription
  As a content creator
  I need to choose a date and price change
  
  Scenario:
    Given I am on the content creator setting page
    And I am the owner content creator
    And I have edited the price information
    And I have inserted an available date 
    When I press the save change button
    And I press "Yes" on the change confirmation pop-up
    Then my price change should be registered
    And all my clients should receive a message about the price change

  Scenario:
    Given I am on the content creator setting page
    And I am the owner content creator
    And I have edited the price information
    And I have inserted an available date 
    When I press the save change button
    And I press "No" on the change confirmation pop-up
    Then my price change will not be registered

