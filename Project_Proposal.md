# Project Proposal : Subscription and access Website

### George
### Mohamed 

##### **Feature list:**

We intend to create a type of subscribtion based website, where users can subscribe to a seller's page and have access to their content monthly. The features to be implemented are:

The abilities for a user to..
* Have a profile picture
* Change their user name
* Search through content creator
* Add a content creator to the list of subscriptions
* Remove a content creator for the list of subscription
* Comment on a subscribed creator's page
* Rate a content creator
* Record Card Info
* Enable or disable automatic subscription
* Perform a Monthly or Yearly payment

There are also the abilities for a content creator to...
* Modify a page
* Add content to the page
* Put content description of your page
* Add, modify and delete a free preview of your page
* Receive request from subscribers 
* I can view the amount of subscribers I currently have
* Set and change subscription price for months that are 2 months away
